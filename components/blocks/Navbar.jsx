import React from 'react'

import A from '../chunks/A/A'

const Navbar = () => {
  return (
    <>
      <div className="navbar">
        <A href={`/`} text={'main page'} />
        <A href={`/users`} text={'users'} />
      </div>
      <style jsx>
        {`
          .navbar {
            background: orange;
            padding: 15px;
          }
        `}
      </style>
    </>
  )
}

export default Navbar
