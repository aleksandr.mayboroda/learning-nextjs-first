import React from 'react'
import Head from 'next/head'

const Seo = ({
  description,
  title = 'Default title',
  icon = '/favicon.ico',
  keywords = [],
}) => {
  return (
    <Head>
      <title>{title}</title>
      {description && <meta name="description" content={description} />}
      {keywords.length > 0 && <meta keywords={`next, alex, react, ${keywords.join(', ')}`} />}
      <link rel="icon" href={icon} />
    </Head>
  )
}

export default Seo
