import React from 'react'

import Seo from '../chunks/Seo/Seo'
import Navbar from '../blocks/Navbar'

const MainLayout = ({ children, seo = {} }) => {
  return (
    <>
     <Seo {...seo} />
      <Navbar />
      <main>{children}</main>
    </>
  )
}

export default MainLayout
