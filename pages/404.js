import React from 'react'

const NotExitsPage = () => {
  return (
    <div>
      <h1>404</h1>
      <h2>{"Page doesn't exist"}</h2>
    </div>
  )
}

export default NotExitsPage
