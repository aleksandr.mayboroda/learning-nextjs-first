import MainLayout from '../components/layouts/MainLayout'

export default function Home({ seo }) {
  return (
    <MainLayout
      seo={{ title: 'Main page', keywords: ['main page', 'hello world'] }}
    >
      <main>
        <h1>Main page</h1>
      </main>
    </MainLayout>
  )
}
