import A from '../components/chunks/A/A'
import MainLayout from '../components/layouts/MainLayout'

import styles from '../styles/Users.module.scss'

const Users = ({ userList }) => {
  const list =
    !userList.length > 0
      ? null
      : userList.map(({ id, name }) => (
          <li key={id}>
            <A href={`/users/${id}`} text={name} />
          </li>
        ))

  return (
    <MainLayout seo={{ title: 'Users list page', keywords: ['users list'] }}>
      <div className={styles['user-list']}>
        <h1>users list</h1>
        <ul>{list}</ul>
      </div>
    </MainLayout>
  )
}

export async function getStaticProps(context) {
  const result = await fetch('https://jsonplaceholder.typicode.com/users')
  const data = await result.json()
  return {
    props: { userList: data }, // will be passed to the page component as props
  }
}

export default Users
