import React from 'react'

import styles from '../../styles/User.module.scss'

import MainLayout from '../../components/layouts/MainLayout'

const User = ({ user }) => {
  console.log(user)
  return (
    <MainLayout
      seo={{
        title: `${user.name}'s page`,
        keywords: [user.name, user.username],
      }}
    >
      <div className={styles.user}>
        <h1>User {user.id} </h1>
        <p> name {user.name}</p>
      </div>
    </MainLayout>
  )
}

export async function getServerSideProps({ params }) {
  const { id } = params
  const result = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
  const data = await result.json()
  return {
    props: { user: data }, // will be passed to the page component as props
  }
}

export default User
